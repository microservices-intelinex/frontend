import { Routes, Route, Outlet } from "react-router-dom";
import "./App.css";
import Register from "./auth/components/Register";
import Login from "./auth/components/Login";
import NotFound from "./common/components/NotFound";
import Container from "react-bootstrap/esm/Container";
import { MainLayout } from "./books/components/MainLayout";
import BookCreate from "./books/components/BookCreate";
import BookList from "./books/components/BookList";
import { createContext, useState } from "react";

export const BooksContext = createContext();
function App() {
  const [books, setBooks] = useState({
    results: [],
    limit: 100,
    total: 0,
    currentPage: 1,
    totalPages: 0,
    hasNext: false,
  });
  return (
    <div>
      <Routes>
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route
          element={
            <BooksContext.Provider value={[books, setBooks]}>
              <MainLayout />{" "}
            </BooksContext.Provider>
          }
        >
          <Route
            path="/books"
            element={
              <BooksContext.Provider value={[books, setBooks]}>
                <BookList />
              </BooksContext.Provider>
            }
          />
          <Route
            path="/books/:id/rating"
            element={
              <BooksContext.Provider value={[books, setBooks]}>
                <BookList />
              </BooksContext.Provider>
            }
          />

          <Route path="/books/create" element={<BookCreate />} />
        </Route>
        <Route path="*" element={<NotFound />} />
      </Routes>
    </div>
  );
}
export default App;
