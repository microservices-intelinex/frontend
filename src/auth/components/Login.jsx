import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Alert from "react-bootstrap/Alert";
import { useState } from "react";
import axios from "axios";
import { config } from "./../../config";
import { useNavigate, Link } from "react-router-dom";
export default function Login() {
  const navigate = useNavigate();
  const [validated, setValidated] = useState(false);
  const [error, setError] = useState(false);
  const handleSubmit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    const form = event.currentTarget;
    if (form.checkValidity() === true) {
      axios
        .post(
          config.urlApi + "auth/login",
          {
            email: form.email.value,
            password: form.password.value,
          },
          {
            method: "POST",
          }
        )
        .then(({ data: { access_token } }) => {
          localStorage.setItem("access_token", access_token);
          navigate("/books");
        })
        .catch((e) => {
          console.error(e);
          setError(true);
        });
    }

    setValidated(true);
  };

  return (
    <Row>
      <Col md></Col>
      <Col md>
        <Card className="mt-3">
          <Card.Body>
            <Card.Title className="text-center">Login</Card.Title>
            <Card.Text>
              {error ? (
                <Alert variant="danger"> Opp! Error de auntenticacion</Alert>
              ) : (
                ""
              )}
              <Form noValidate validated={validated} onSubmit={handleSubmit}>
                <Row>
                  <Form.Group as={Col} controlId="email">
                    <Form.Label>Email</Form.Label>
                    <Form.Control
                      required
                      type="email"
                      placeholder="juan@gmail.com"
                    />
                    <Form.Control.Feedback type="invalid">
                      Ingresa un correo valido
                    </Form.Control.Feedback>
                  </Form.Group>
                </Row>
                <Row className="mb-3">
                  <Form.Group as={Col} controlId="password">
                    <Form.Label>Contraseña</Form.Label>
                    <Form.Control
                      required
                      type="password"
                      placeholder="*****"
                    />
                    <Form.Control.Feedback type="invalid">
                      Ingresa una constraseña valida
                    </Form.Control.Feedback>
                  </Form.Group>
                </Row>
                <div className="text-center">
                  <Button type="submit">Ingresar</Button>
                </div>
                <Link to="/register">Quiero registrarme</Link>
              </Form>
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col md></Col>
    </Row>
  );
}
