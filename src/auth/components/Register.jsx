import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Alert from "react-bootstrap/Alert";
import { useState } from "react";
import axios from "axios";
import { config } from "./../../config";
import { useNavigate, Link } from "react-router-dom";

export default function Login() {
  const navigate = useNavigate();
  const [validated, setValidated] = useState(false);
  const [error, setError] = useState("");
  const handleSubmit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    const form = event.currentTarget;
    if (form.checkValidity() === true) {
      axios
        .post(
          config.urlApi + "auth/register",
          {
            identification: form.identification.value,
            age: parseInt(form.age.value) || 0,
            location: form.location.value,
            email: form.email.value,
            password: form.password.value,
          },
          {
            method: "POST",
          }
        )
        .then(() => {
          alert("Has sido registrado correctamente");
          navigate("/login");
        })
        .catch((e) => {
          console.error(e);
          setError(e?.response?.data?.message || "Error de registro");
        });
    }

    setValidated(true);
  };

  return (
    <Row>
      <Col md></Col>
      <Col md>
        <Card className="mt-3">
          <Card.Body>
            <Card.Title className="text-center">Registro</Card.Title>
            <Card.Text>
              {error ? <Alert variant="danger"> Opp!{error}</Alert> : ""}
              <Form noValidate validated={validated} onSubmit={handleSubmit}>
                <Row>
                  <Form.Group as={Col} controlId="identification">
                    <Form.Label>Identificacion</Form.Label>
                    <Form.Control
                      required
                      type="text"
                      placeholder="100012121"
                    />
                    <Form.Control.Feedback type="invalid">
                      Ingresa una identificacion
                    </Form.Control.Feedback>
                  </Form.Group>
                </Row>
                <Row>
                  <Form.Group as={Col} controlId="location">
                    <Form.Label>Ubicacion</Form.Label>
                    <Form.Control
                      required
                      type="text"
                      placeholder="Bogota DC"
                    />
                    <Form.Control.Feedback type="invalid">
                      Ingresa una ubicacion
                    </Form.Control.Feedback>
                  </Form.Group>
                </Row>
                <Row>
                  <Form.Group as={Col} controlId="age">
                    <Form.Label>Edad</Form.Label>
                    <Form.Control required type="number" placeholder="34" />
                    <Form.Control.Feedback type="invalid">
                      Ingresa tu edad
                    </Form.Control.Feedback>
                  </Form.Group>
                </Row>
                <Row>
                  <Form.Group as={Col} controlId="email">
                    <Form.Label>Email</Form.Label>
                    <Form.Control
                      required
                      type="email"
                      placeholder="juan@gmail.com"
                    />
                    <Form.Control.Feedback type="invalid">
                      Ingresa un correo valido
                    </Form.Control.Feedback>
                  </Form.Group>
                </Row>

                <Row className="mb-3">
                  <Form.Group as={Col} controlId="password">
                    <Form.Label>Contraseña</Form.Label>
                    <Form.Control
                      required
                      type="password"
                      placeholder="*****"
                    />
                    <Form.Control.Feedback type="invalid">
                      Ingresa una constraseña valida
                    </Form.Control.Feedback>
                  </Form.Group>
                </Row>
                <div className="text-center">
                  <Button type="submit">Loguearme</Button>
                </div>
                <Link to="/login">Ya tengo una cuenta</Link>
              </Form>
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
      <Col md></Col>
    </Row>
  );
}
