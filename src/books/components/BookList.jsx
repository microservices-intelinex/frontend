import { useEffect } from "react";
import { useContext } from "react";
import { Link } from "react-router-dom";
import { BooksContext } from "../../App";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Pagination from "react-bootstrap/Pagination";
import Button from "react-bootstrap/esm/Button";
import { findBooks, removeBook } from "../book.service";
export default function BookList() {
  const [books, setBooks] = useContext(BooksContext);
  const loadBooks = async () => setBooks(await findBooks());
  const handleDelete = async (id) => {
    if (confirm("Estas seguro?")) {
      await removeBook(id);
      loadBooks();
    }
  };
  const handlePaginatePrev = async () => {
    setBooks(await findBooks({ page: books.currentPage - 1 }));
  };
  const handlePaginateNext = async () => {
    setBooks(await findBooks({ page: books.currentPage + 1 }));
  };
  return (
    <>
      <div>
        <Link to="/books/create" className="btn btn-info">
          Create a book
        </Link>
      </div>
      <div className="text-center">
        Page {books.currentPage} | {books.limit * books.currentPage} de{" "}
        {books.total} resultados
        <Pagination>
          <Pagination.Prev
            disabled={books.currentPage - 1 <= 0}
            onClick={() => handlePaginatePrev()}
          />
          <Pagination.Next
            disabled={books.currentPage > books.totalPages}
            onClick={() => handlePaginateNext()}
          />
        </Pagination>
      </div>
      <Row xs={1} md={4} className="g-4">
        {books.results.map(
          ({ imgUrlL, title, author, publishedYear, publisher, ISBN, _id }) => (
            <Col key={ISBN}>
              <Card>
                <Card.Img variant="top" src={imgUrlL} />
                <Card.Body>
                  <Card.Title>{title}</Card.Title>
                  <Card.Text>
                    ISBN {ISBN}
                    <br />
                    Publicado por, <b>{publisher}</b>
                  </Card.Text>
                </Card.Body>
                <Card.Footer className="px-auto">
                  <Button
                    className="btn btn-danger float-right mr-4"
                    onClick={() => handleDelete(_id)}
                  >
                    x
                  </Button>
                  <small className="mx-auto">
                    {author} - {publishedYear}
                  </small>
                </Card.Footer>
              </Card>
            </Col>
          )
        )}
      </Row>
    </>
  );
}
