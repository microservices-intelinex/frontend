import Navbar from "./Navbar";
import { Outlet, useNavigate } from "react-router-dom";
import { useContext, useEffect, useState } from "react";
import jwt from "jwt-decode";
import { config } from "./../../config";
import axios from "axios";
import { BooksContext } from "../../App";
import { findBooks } from "../book.service";

export function MainLayout() {
  const navigate = useNavigate();
  const [user, setUser] = useState({});
  const [books, setBooks] = useContext(BooksContext);
  const loadBooks = async () => setBooks(await findBooks());
  useEffect(() => {
    const access_token = localStorage.getItem("access_token");
    if (!access_token) {
      return navigate("/login");
    }
    try{

      setUser(jwt(access_token));
      loadBooks();
    }catch(err){
      localStorage.removeItem("access_token")
      return navigate("/login");
    }
  }, []);
  return (
    <div>
      <Navbar user={user}></Navbar>
      <Outlet user={user} />
    </div>
  );
}
