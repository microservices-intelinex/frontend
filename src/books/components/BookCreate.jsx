import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Alert from "react-bootstrap/Alert";
import { useState } from "react";
import axios from "axios";
import { config } from "./../../config";
import { useNavigate, Link } from "react-router-dom";
import { getHeaders } from "../book.service";
import toastr from "toastr";
export default function BookCreate() {
  const navigate = useNavigate();
  const [validated, setValidated] = useState(false);
  const [error, setError] = useState("");
  const handleSubmit = (event) => {
    event.preventDefault();
    event.stopPropagation();
    const form = event.currentTarget;
    if (form.checkValidity() === true) {
      axios
        .post(
          config.urlApi + "books",
          {
            publisher: form.publisher.value,
            publishedYear: parseInt(form.publishedYear.value) || 0,
            author: form.author.value,
            title: form.title.value,
            ISBN: form.ISBN.value,
            imgUrlS: form.imgUrlS.value,
            imgUrlL: form.imgUrlL.value,
            imgUrlM: form.imgUrlM.value,
          },
          {
            method: "POST",
            ...getHeaders(),
          }
        )
        .then(({ data: { access_token } }) => {
          toastr.info("Book created successfully");
        })
        .catch((e) => {
          console.error(e);
          setError(
            e?.response?.data?.message.join(" ** ") || "Error de registro"
          );
        });
    }

    setValidated(true);
  };

  return (
    <div>
      <Link to="/books" className="btn btn-info">
        Regresar
      </Link>
      <Row>
        <Col md={3}></Col>
        <Col md={6}>
          <Card className="mt-3">
            <Card.Body>
              <Card.Title className="text-center">Crear libro</Card.Title>
              <Card.Text>
                {!!error ? <Alert variant="danger"> Opp! {error}</Alert> : ""}
                <Form
                  id="form-search"
                  noValidate
                  validated={validated}
                  onSubmit={handleSubmit}
                >
                  <Row>
                    <Form.Group as={Col} controlId="ISBN">
                      <Form.Label>ISBN</Form.Label>
                      <Form.Control type="text" />
                    </Form.Group>
                  </Row>
                  <Row>
                    <Form.Group as={Col} controlId="title">
                      <Form.Label>Titulo</Form.Label>
                      <Form.Control type="text" />
                    </Form.Group>
                  </Row>
                  <Row>
                    <Form.Group as={Col} controlId="author">
                      <Form.Label>Autor</Form.Label>
                      <Form.Control type="text" />
                    </Form.Group>
                  </Row>
                  <Row>
                    <Form.Group as={Col} controlId="publisher">
                      <Form.Label>Editorial</Form.Label>
                      <Form.Control type="text" />
                    </Form.Group>
                  </Row>
                  <Row>
                    <Form.Group as={Col} controlId="publishedYear">
                      <Form.Label>Año de publicacion</Form.Label>
                      <Form.Control type="text" />
                    </Form.Group>
                  </Row>
                  <Row>
                    <Form.Group as={Col} controlId="imgUrlS">
                      <Form.Label>Url imagen Pequeña</Form.Label>
                      <Form.Control type="text" />
                    </Form.Group>
                  </Row>
                  <Row>
                    <Form.Group as={Col} controlId="imgUrlM">
                      <Form.Label>Url imagen Mediana</Form.Label>
                      <Form.Control type="text" />
                    </Form.Group>
                  </Row>
                  <Row>
                    <Form.Group as={Col} controlId="imgUrlL">
                      <Form.Label>Url imagen Grande</Form.Label>
                      <Form.Control type="text" />
                    </Form.Group>
                  </Row>
                  <Button type="submit">Crear book</Button>
                </Form>
              </Card.Text>
            </Card.Body>
          </Card>
        </Col>
        <Col md={3}></Col>
      </Row>
    </div>
  );
}
