import { useContext, useEffect, useState } from "react";
import { useRef } from "react";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import NavDropdown from "react-bootstrap/NavDropdown";
import Offcanvas from "react-bootstrap/Offcanvas";
import { useNavigate } from "react-router-dom";
import { fromEvent, debounceTime } from "rxjs";
import { BooksContext } from "../../App";
import { findBooks, findBooksSearchBar } from "../book.service";
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import Card from "react-bootstrap/Card";
import Alert from "react-bootstrap/Alert";


export default function NavbarApp({ user: { email } }) {
  const navigate = useNavigate();
  const ref = useRef(null);
  const expand = "md";
  const [books, setBooks] = useContext(BooksContext);
  const [show, setShow] = useState(false);
  useEffect(() => {
    fromEvent(ref.current, "input")
      .pipe(debounceTime(500))
      .subscribe(async ({ target: { value } }) => {
        setBooks(await findBooksSearchBar(value));
      });
  }, []);

  const handleCloseSesion = () => {
    localStorage.removeItem("access_token");
    navigate("/login");
  };
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [validated, setValidated] = useState(false);
  const handleSubmit = async (event) => {
    event.preventDefault();
    event.stopPropagation();
    const form = event.currentTarget;
    if (form.checkValidity() === true) {
      const payload = {
        publisher: form.publisher.value,
        publishedYear: parseInt(form.publishedYear.value) || 0,
        author: form.author.value,
        title: form.title.value,
        ISBN: form.ISBN.value,
      };
      setBooks(await findBooks(payload));
      setShow(false)
    }

    setValidated(true);
  };
  return (
    <>
      <Navbar bg="light" className="mb-3">
        <Container fluid>
          <Navbar.Brand href="#">InteliNex</Navbar.Brand>
          <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-${expand}`} />
          <Navbar.Offcanvas
            id={`offcanvasNavbar-expand-${expand}`}
            aria-labelledby={`offcanvasNavbarLabel-expand-${expand}`}
            placement="end"
          >
            <Offcanvas.Header closeButton>
              <Offcanvas.Title id={`offcanvasNavbarLabel-expand-${expand}`}>
                Offcanvas
              </Offcanvas.Title>
            </Offcanvas.Header>
            <Offcanvas.Body>
              <Form className="d-flex w-100">
                <Form.Control
                  ref={ref}
                  type="search"
                  placeholder="searcher"
                  className="me-2"
                  aria-label="searcher"
                />
                <Button
                  variant="success"
                  style={{ width: "170px" }}
                  className="btn btn-sm"
                  onClick={handleShow}
                >
                  Busqueda detallada
                </Button>
              </Form>
              <Nav className="justify-content-end flex-grow-1 pe-3">
                <NavDropdown
                  title={email}
                  id={`offcanvasNavbarDropdown-expand-${expand}`}
                >
                  <NavDropdown.Item onClick={handleCloseSesion}>
                    Cerrar sesion
                  </NavDropdown.Item>
                </NavDropdown>
              </Nav>
            </Offcanvas.Body>
          </Navbar.Offcanvas>
        </Container>
      </Navbar>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Busqueta por campós en detalle</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form
            id="form-search"
            noValidate
            validated={validated}
            onSubmit={handleSubmit}
          >
            <Row>
              <Form.Group as={Col} controlId="publisher">
                <Form.Label>Editorial</Form.Label>
                <Form.Control type="text" />
              </Form.Group>
            </Row>
            <Row>
              <Form.Group as={Col} controlId="publishedYear">
                <Form.Label>Año de publicacion</Form.Label>
                <Form.Control type="text" />
              </Form.Group>
            </Row>
            <Row>
              <Form.Group as={Col} controlId="author">
                <Form.Label>Autor</Form.Label>
                <Form.Control type="text" />
              </Form.Group>
            </Row>
            <Row>
              <Form.Group as={Col} controlId="title">
                <Form.Label>Titulo</Form.Label>
                <Form.Control type="text" />
              </Form.Group>
            </Row>
            <Row>
              <Form.Group as={Col} controlId="ISBN">
                <Form.Label>ISBN</Form.Label>
                <Form.Control type="text" />
              </Form.Group>
            </Row>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button type="submit" form="form-search" variant="primary">
            Buscar
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}
