import axios from "axios";
import { config } from "../config";
import querystring from "query-string";
export const getHeaders = () => ({
  headers: {
    Authorization: "Bearer " + localStorage.getItem("access_token"),
  },
});

export const findBooksSearchBar = async (value) => {
  try {
    const { data } = await axios.get(
      config.urlApi + (value ? "books/searcher/" + value : "books"),
      getHeaders()
    );
    return data;
  } catch (err) {
    console.error(err);
    throw new Error(err);
  }
};

export const findBooks = async (params = {}) => {
  try {
    const query = Object.fromEntries(
      Object.entries(params).filter(([k, v]) => !!v)
    );
    const { data } = await axios.get(
      config.urlApi + "books?" + querystring.stringify(query),
      getHeaders()
    );
    return data;
  } catch (err) {
    console.error(err);
    throw new Error(err);
  }
};

export const removeBook = async (id) => {
  try {
    await axios.delete(config.urlApi + "books/" + id, getHeaders());
  } catch (err) {
    console.error(err);
    throw new Error(err);
  }
};
